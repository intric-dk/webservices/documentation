# Documentation

This repository contains the documentation for the Intric webservices.

## License

This project is licensed under the terms of the [MIT license](./LICENSE.md).
